
import controlefinancas.ControleSaldo;
import java.util.ArrayList;
import models.Despesa;
import models.Renda;
import models.Usuario;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class testControleSaldo {
    
    public testControleSaldo() {
    }
    
    private ControleSaldo umControle;
    private Despesa umaDespesa;
    private Renda umaRenda;
    private ArrayList<Usuario> listaSaldo;

	
	@Before
	public void setUp()throws Exception{
            umControle = new ControleSaldo();
            umaDespesa = new Despesa();
            umaRenda = new Renda();
            listaSaldo = new ArrayList<Usuario>();
            
	}

	@Test
	public void testAdicionarRenda() {
            umaRenda.setData("11/09/2001");
            umControle.adicionarRenda(umaRenda);
            
            assertEquals("11/09/2001", umaRenda.getData());
            
	}
        
        @Test
	public void testAdicionarDespesa() {
            umaDespesa.setData("21/12/2012");
            umControle.adicionarDespesa(umaDespesa);
            
            assertEquals("21/12/2012", umaDespesa.getData());
            
	}
	

}
