
import controlefinancas.ControleUsuario;
import java.util.ArrayList;
import models.Usuario;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class testControleUsuario {
    
    public testControleUsuario() {
    }

    private ControleUsuario umControle;
    private Usuario umUsuario;
    private ArrayList<Usuario> listaUsuario;

	
	@Before
	public void setUp()throws Exception{
            umControle = new ControleUsuario();
            umUsuario = new Usuario();
            listaUsuario = new ArrayList<Usuario>();
            
	}

	@Test
	public void testAdicionar() {
            umUsuario.setNome("José");
            umControle.adicionar(umUsuario);
            
            assertEquals("José", umUsuario.getNome());
            
	}
	
	
	@Test
	public void testPesquisarLogin() {
		String umLogin = "Matheus";
		umUsuario.setLogin(umLogin);
		umControle.adicionar(umUsuario);
		
		assertEquals(umUsuario, umControle.pesquisarLogin(umLogin));
		
	}
    
}
