
package controlefinancas;

import java.util.ArrayList;
import models.Usuario;

public class ControleUsuario {
    
    private ArrayList<Usuario> listaUsuarios;

    public ControleUsuario() {
        this.listaUsuarios = new ArrayList<>();
    }
    
    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }
    
    public void adicionar(Usuario umUsuario) {
        listaUsuarios.add(umUsuario);
    }

    
    public Usuario pesquisarLogin(String login) {
        for (Usuario u: listaUsuarios) {
            if (u.getLogin().equals(login)) return u;
        }
        return null;
    }

}
