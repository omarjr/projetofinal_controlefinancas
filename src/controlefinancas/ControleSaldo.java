
package controlefinancas;

import java.util.ArrayList;
import models.Despesa;
import models.Renda;
import models.Usuario;

public class ControleSaldo {
    
    private ArrayList<Usuario> listaSaldo;

    public ControleSaldo() {
        this.listaSaldo= new ArrayList<>();
    }

    
    public void setListaSaldo(ArrayList<Usuario> listaSaldo) {
        this.listaSaldo = listaSaldo;
    }
    
    public ArrayList<Usuario> getListaSaldo() {
        return listaSaldo;
    }
    
    public void adicionarRenda(Renda umaRenda) {
        listaSaldo.add(umaRenda);
    }
    
    public void adicionarDespesa(Despesa umaDespesa) {
        listaSaldo.add(umaDespesa);
    }
    
    
    public double calcularSaldo(Usuario usuario, Renda renda, Despesa despesa){
        double saldoAtualizado = 0;
        
        if(renda != null) {
            saldoAtualizado = usuario.getSaldo();
            saldoAtualizado += renda.getValorRecebido();
        }
        
        if(despesa != null) {
            saldoAtualizado = usuario.getSaldo();
            saldoAtualizado -= despesa.getValorRetirado();
        }
        return saldoAtualizado;
    }
    
}
