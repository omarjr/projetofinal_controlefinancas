
package models;


public class SubcategoriaDespesa {
    
    private int subcategoria;
    
    public int getSubcategoria(){
        return subcategoria;  
    }

    public void setSubcategoria(int subcategoria) {
        this.subcategoria = subcategoria;
    }
    
    public String obterSubcategoria() {
        return obterSubcategoria(this.getSubcategoria());
    }

    public static String obterSubcategoria(int Subcategoria) {
        
        if (Subcategoria == 0) {
            return "Alimentação";
        } else if (Subcategoria == 1) {
            return "Saúde";
        } else if (Subcategoria == 2) {
            return "Educação" ;
        } else if (Subcategoria == 3) {
            return "Entreterimento";
        }  else if (Subcategoria == 4) {
            return "Transporte";
        } else {
            return "Outras Despesas";
        }
        
    }
      
}
