
package models;

import java.util.ArrayList;

public class Usuario {
    private String nome;
    private String login;
    private String senha;
    protected Double saldo = 0.0;
    protected String ident;
    private ArrayList<Usuario> listaTransacoes;
  
    
    public Usuario (){
        this.ident = "Usuario";
        this.listaTransacoes= new ArrayList<>();
        }
    

    public ArrayList<Usuario> getListaTransacoes() {
        return listaTransacoes;
    }

    public void setListaTransacoes(ArrayList<Usuario> listaTransacoes) {
        this.listaTransacoes = listaTransacoes;
    }
    
    
    public String getIdent(){
        return ident;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
    
}
