
package models;

public class Renda extends Usuario {
    private Double valorRecebido;
    private String data;
    private SubcategoriaRenda subcategoria;
    
    public Renda (){
        this.ident = "Renda";
    }

    public Double getValorRecebido() {
        return valorRecebido;
    }

    public void setValorRecebido(Double valorRecebido) {
        this.valorRecebido = valorRecebido;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public SubcategoriaRenda getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(SubcategoriaRenda subcategoria) {
        this.subcategoria = subcategoria;
    }
    
}
