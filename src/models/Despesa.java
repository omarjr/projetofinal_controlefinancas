
package models;

public class Despesa extends Usuario{
    private Double valorRetirado;
    private String data;
    private SubcategoriaDespesa subcategoria;
    
    public Despesa() {
        this.ident = "Despesa";
        }
    

    public Double getValorRetirado() {
        return valorRetirado;
    }

    public void setValorRetirado(Double valorRetirado) {
        this.valorRetirado = valorRetirado;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public SubcategoriaDespesa getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(SubcategoriaDespesa subcategoria) {
        this.subcategoria = subcategoria;
    }
     
}
