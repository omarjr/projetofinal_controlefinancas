
package models;

public class SubcategoriaRenda {
  
    private int subcategoria;
    
    public int getSubcategoria(){
        return subcategoria;  
    }

    public void setSubcategoria(int subcategoria) {
        this.subcategoria = subcategoria;
    }
    
    public String obterSubcategoria() {
        return obterSubcategoria(this.getSubcategoria());
    }

    public static String obterSubcategoria(int Subcategoria) {
        
        if (Subcategoria == 0) {
            return "Salário";
        } else if (Subcategoria == 1) {
            return "Bonificação";
        } else if (Subcategoria == 2) {
            return "Venda" ;
        } else {
            return "Pensão";
        }
        
    }
    
}
