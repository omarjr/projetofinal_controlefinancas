

package tela;

import javax.swing.JOptionPane;


public class TelaAdicionarRenda extends javax.swing.JDialog {

   
    public TelaAdicionarRenda(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
     
    private int INDICE;
    private double valor;
    private String data;

    public String getData() {
        return data;
    }

    public int getINDICE() {
        return INDICE;
    }

    public double getValor() {
        return valor;
    }

    public void cancelar() {
               
        int escolha = JOptionPane.showOptionDialog(null,
            "Todos os dados serão perdidos. Deseja continuar?",
            "Cancelar?",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,null,null);
        
        if(escolha == JOptionPane.YES_OPTION)
            this.setVisible(false);
    }
  

    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private boolean validarCampos() {
        
        if (jTextFieldValor.getText().trim().length() == 0) {
            this.exibirInformacao("É obrigatório o preenchimento do campo 'Valor'");
            jTextFieldValor.requestFocus();
            return false;
        }
      
        if (jFormattedTextFieldData.getText().length() == 0){
            this.exibirInformacao("É obrigatório o preenchimento do campo 'Data'");
            jFormattedTextFieldData.requestFocus();
            return false;
        }
         
        return true;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBoxSubcategoria = new javax.swing.JComboBox();
        jLabelNovaRenda = new javax.swing.JLabel();
        jLabelSubCategoria = new javax.swing.JLabel();
        jLabelValor = new javax.swing.JLabel();
        jLabelData = new javax.swing.JLabel();
        jTextFieldValor = new javax.swing.JTextField();
        jButtonAdicionar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jFormattedTextFieldData = new javax.swing.JFormattedTextField();
        jLabelPlanoDeFundo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("+ Renda");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jComboBoxSubcategoria.setBackground(new java.awt.Color(78, 254, 75));
        jComboBoxSubcategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Salário", "Bonificação", "Venda", "Pensão" }));
        jComboBoxSubcategoria.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jComboBoxSubcategoria.setPreferredSize(null);
        jComboBoxSubcategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxSubcategoriaActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxSubcategoria, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 60, -1, -1));

        jLabelNovaRenda.setFont(new java.awt.Font("Ubuntu", 0, 20)); // NOI18N
        jLabelNovaRenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tela/moneyalterado.png"))); // NOI18N
        jLabelNovaRenda.setText("Nova Renda");
        getContentPane().add(jLabelNovaRenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, -1, -1));

        jLabelSubCategoria.setBackground(new java.awt.Color(206, 122, 38));
        jLabelSubCategoria.setText("Subcategoria:");
        jLabelSubCategoria.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabelSubCategoria.setPreferredSize(null);
        getContentPane().add(jLabelSubCategoria, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, -1, -1));

        jLabelValor.setBackground(new java.awt.Color(206, 122, 38));
        jLabelValor.setText("Valor:");
        jLabelValor.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabelValor.setPreferredSize(null);
        getContentPane().add(jLabelValor, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, -1, -1));

        jLabelData.setBackground(new java.awt.Color(206, 122, 38));
        jLabelData.setText("Data:");
        jLabelData.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabelData.setPreferredSize(null);
        getContentPane().add(jLabelData, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 150, -1, -1));

        jTextFieldValor.setBackground(new java.awt.Color(150, 254, 148));
        jTextFieldValor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldValor.setToolTipText("ex. 1250.90");
        jTextFieldValor.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        getContentPane().add(jTextFieldValor, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 100, 80, -1));
        jTextFieldValor.getAccessibleContext().setAccessibleDescription("ex. 1250.90");

        jButtonAdicionar.setText("Adicionar");
        jButtonAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonAdicionar, new org.netbeans.lib.awtextra.AbsoluteConstraints(57, 192, 188, -1));

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(57, 234, 188, -1));

        jFormattedTextFieldData.setBackground(new java.awt.Color(150, 254, 148));
        try {
            jFormattedTextFieldData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        getContentPane().add(jFormattedTextFieldData, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 140, 80, -1));

        jLabelPlanoDeFundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tela/planodefundoAdicionarRenda.png"))); // NOI18N
        getContentPane().add(jLabelPlanoDeFundo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxSubcategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxSubcategoriaActionPerformed
      
    }//GEN-LAST:event_jComboBoxSubcategoriaActionPerformed

    private void jButtonAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarActionPerformed
      
        if (this.validarCampos() == false) {
            return;
        }
        
        this.INDICE = jComboBoxSubcategoria.getSelectedIndex();
        
        try {
            this.valor = Double.parseDouble(jTextFieldValor.getText());
        } catch (NumberFormatException ex) {
            this.exibirInformacao("O valor do campo 'Valor' é inválido.");
            jTextFieldValor.requestFocus();
            return;
        }
        
       
          
        this.data = jFormattedTextFieldData.getText();
                  
        this.setVisible(false); 
    }//GEN-LAST:event_jButtonAdicionarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        this.cancelar();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

  
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaAdicionarRenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaAdicionarRenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaAdicionarRenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaAdicionarRenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                TelaAdicionarRenda dialog = new TelaAdicionarRenda(new javax.swing.JDialog(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionar;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JComboBox jComboBoxSubcategoria;
    private javax.swing.JFormattedTextField jFormattedTextFieldData;
    private javax.swing.JLabel jLabelData;
    private javax.swing.JLabel jLabelNovaRenda;
    private javax.swing.JLabel jLabelPlanoDeFundo;
    private javax.swing.JLabel jLabelSubCategoria;
    private javax.swing.JLabel jLabelValor;
    private javax.swing.JTextField jTextFieldValor;
    // End of variables declaration//GEN-END:variables
}
