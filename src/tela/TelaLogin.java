
package tela;

import controlefinancas.ControleUsuario;
import javax.swing.JOptionPane;
import models.Usuario;

public class TelaLogin extends javax.swing.JFrame {

    private Usuario umUsuario;
    private String nome;
    private String login;
    private String senha;
    private final ControleUsuario controleUsuario;

    
    
    public TelaLogin() {
        initComponents();
        setLocationRelativeTo(null);
        this.controleUsuario = new ControleUsuario();
        this.umUsuario = new Usuario();
    }
    
    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private boolean validarCampos() {
        
        senha = new String(jPasswordFieldSenha.getPassword());
        
        if (jTextFieldLogin.getText().trim().length() == 0) {
            this.exibirInformacao("É obrigatório o preenchimento do campo 'Login'");
            jTextFieldLogin.requestFocus();
            return false;
        }
       
        if (senha.trim().length() == 0) {
            this.exibirInformacao("É obrigatório o preenchimento do campo 'Senha'");
            jTextFieldLogin.requestFocus();
            return false;
        }
       
        return true;
    }
     
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelLogin = new javax.swing.JLabel();
        jLabelImagem = new javax.swing.JLabel();
        jLabelSenha = new javax.swing.JLabel();
        jTextFieldLogin = new javax.swing.JTextField();
        jLabelNaoCadastrou = new javax.swing.JLabel();
        jLabelCadastre = new javax.swing.JLabel();
        jButtonFazerLogin = new javax.swing.JButton();
        jPasswordFieldSenha = new javax.swing.JPasswordField();
        jLabelPlanoDeFundo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tela de Login");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelLogin.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabelLogin.setText("Login:");
        getContentPane().add(jLabelLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 210, 70, 33));

        jLabelImagem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tela/financas.png"))); // NOI18N
        getContentPane().add(jLabelImagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 700, 250));

        jLabelSenha.setFont(new java.awt.Font("Khmer OS System", 1, 18)); // NOI18N
        jLabelSenha.setText("Senha:");
        getContentPane().add(jLabelSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 280, 70, 40));
        getContentPane().add(jTextFieldLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 240, 280, 30));

        jLabelNaoCadastrou.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jLabelNaoCadastrou.setText("Ainda não se cadastrou?");
        getContentPane().add(jLabelNaoCadastrou, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 410, -1, -1));

        jLabelCadastre.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jLabelCadastre.setForeground(new java.awt.Color(0, 25, 255));
        jLabelCadastre.setText(" Cadastre-se já");
        jLabelCadastre.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelCadastre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelCadastreMouseClicked(evt);
            }
        });
        getContentPane().add(jLabelCadastre, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 410, 120, -1));

        jButtonFazerLogin.setBackground(new java.awt.Color(108, 164, 253));
        jButtonFazerLogin.setFont(new java.awt.Font("Khmer OS System", 0, 15)); // NOI18N
        jButtonFazerLogin.setText("Fazer login");
        jButtonFazerLogin.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonFazerLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFazerLoginActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonFazerLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 360, 280, 30));
        getContentPane().add(jPasswordFieldSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 310, 280, -1));

        jLabelPlanoDeFundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tela/CAPTURA.png"))); // NOI18N
        jLabelPlanoDeFundo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelPlanoDeFundoMouseClicked(evt);
            }
        });
        getContentPane().add(jLabelPlanoDeFundo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 700, 460));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabelCadastreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelCadastreMouseClicked
        TelaCadastro cadastro = new TelaCadastro(this, true);
        cadastro.setLocationRelativeTo(null);
        cadastro.setVisible(true);
        jTextFieldLogin.setText(null);
        jPasswordFieldSenha.setText(null);
        
        Usuario usuario = new Usuario();
        
        if (cadastro.getNome() != null && cadastro.getLogin() != null && cadastro.getSenha() != null ){

            usuario.setNome(cadastro.getNome());
            usuario.setLogin(cadastro.getLogin());
            usuario.setSenha(cadastro.getSenha());
            controleUsuario.adicionar(usuario);
            
        }
        cadastro.dispose();
    }//GEN-LAST:event_jLabelCadastreMouseClicked

    private void jLabelPlanoDeFundoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelPlanoDeFundoMouseClicked
        
    }//GEN-LAST:event_jLabelPlanoDeFundoMouseClicked

    private void jButtonFazerLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFazerLoginActionPerformed
        
        if (this.validarCampos() == false) {
            return;
        }
        
        Usuario usuarioPesquisado = controleUsuario.pesquisarLogin(jTextFieldLogin.getText());
     
       
        if(usuarioPesquisado == null){
            this.exibirInformacao("Login inexistente. Tente novamente!");
            jTextFieldLogin.setText(null);
            jPasswordFieldSenha.setText(null); 
        }  
       
        if(usuarioPesquisado != null){
            
            if( usuarioPesquisado.getSenha().equals(this.senha = new String(jPasswordFieldSenha.getPassword()))){
                umUsuario = usuarioPesquisado;
                TelaPrincipal principal = new TelaPrincipal(this, true, usuarioPesquisado);
                principal.setLocationRelativeTo(null);
                principal.setVisible(true);
                
                jTextFieldLogin.setText(null);
                jPasswordFieldSenha.setText(null);
                principal.dispose();

            } else{
                  this.exibirInformacao("Senha incorreta. Tente novamente!");
                  jPasswordFieldSenha.setText(null);  
              }
        }
        
    }//GEN-LAST:event_jButtonFazerLoginActionPerformed

   
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaLogin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonFazerLogin;
    private javax.swing.JLabel jLabelCadastre;
    private javax.swing.JLabel jLabelImagem;
    private javax.swing.JLabel jLabelLogin;
    private javax.swing.JLabel jLabelNaoCadastrou;
    private javax.swing.JLabel jLabelPlanoDeFundo;
    private javax.swing.JLabel jLabelSenha;
    private javax.swing.JPasswordField jPasswordFieldSenha;
    private javax.swing.JTextField jTextFieldLogin;
    // End of variables declaration//GEN-END:variables
}

