
package tela;

import controlefinancas.ControleSaldo;
import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import models.Despesa;
import models.Renda;
import models.SubcategoriaDespesa;
import models.SubcategoriaRenda;
import models.Usuario;


public class TelaPrincipal extends javax.swing.JDialog {
    
    private final ControleSaldo controleSaldo;
    private final Usuario umUsuario;
    private double saldo = 0 ;
    private double saldoPositivo = 0;
    private double saldoNegativo = 0;
    private final  DecimalFormat df;
    
    public TelaPrincipal(java.awt.Frame parent, boolean modal, Usuario usuario) {
        super(parent, modal);
        initComponents();
        df = new DecimalFormat("#,##0.00");
        this.controleSaldo = new ControleSaldo();
        this.umUsuario = usuario;
        controleSaldo.setListaSaldo(umUsuario.getListaTransacoes());
        jLabelNomeUsuario.setText(umUsuario.getNome());
        this.carregarLista();
        
        if(umUsuario.getSaldo() < 0){
        jLabelValorSaldo.setForeground( new Color(255, 69, 0));
        jLabelValorSaldo.setText(String.valueOf(df.format(umUsuario.getSaldo())));
        } else{
            jLabelValorSaldo.setForeground(new Color(21, 152, 1));
            jLabelValorSaldo.setText(String.valueOf(df.format(umUsuario.getSaldo())));
          }
        
        jLabelValorRenda.setText(String.valueOf(df.format(saldoPositivo)));
        jLabelValorDespesa.setText(String.valueOf(df.format(saldoNegativo)));
        saldoNegativo = 0;
        saldoPositivo = 0;
    }
    
    
    private void carregarLista(){
        ArrayList<Usuario> listaUsuarios;
        listaUsuarios = controleSaldo.getListaSaldo();
        DefaultTableModel model = (DefaultTableModel) jTableLista.getModel();
        model.setRowCount(0);

        for(Usuario usuario : listaUsuarios){
            
            if(usuario.getIdent().equals("Renda")) {
              
                Renda renda = (Renda) usuario;
                saldoPositivo += renda.getValorRecebido();
                saldo = controleSaldo.calcularSaldo(umUsuario, renda, null);
                
                 if(saldo < 0){
                    jLabelValorSaldo.setForeground(new Color(255, 69, 0));
                    jLabelValorSaldo.setText(String.valueOf(df.format(saldo)));
                    } else{
                        jLabelValorSaldo.setForeground(new Color(21, 152, 1));
                        jLabelValorSaldo.setText(String.valueOf(df.format(saldo)));
                      }
               
                model.addRow(new String[]{renda.getIdent(), renda.getSubcategoria().obterSubcategoria(), String.valueOf(df.format(renda.getValorRecebido())), renda.getData()});
                  
            }   
                
            if(usuario.getIdent().equals("Despesa")) {
                    
                Despesa despesa = (Despesa) usuario;
                saldoNegativo += despesa.getValorRetirado();
                saldo = controleSaldo.calcularSaldo(umUsuario, null , despesa); 
                
                if(saldo < 0){
                    jLabelValorSaldo.setForeground(new Color(255, 69, 0));
                    jLabelValorSaldo.setText(String.valueOf(df.format(saldo)));
                    } else{
                        jLabelValorSaldo.setForeground(new Color(21, 152, 1));
                        jLabelValorSaldo.setText(String.valueOf(df.format(saldo)));
                      }
             
                model.addRow(new String[]{despesa.getIdent(), despesa.getSubcategoria().obterSubcategoria(), String.valueOf(df.format(despesa.getValorRetirado())), despesa.getData()});
                  
            }
            jTableLista.setModel(model);
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelImagem = new javax.swing.JLabel();
        jScrollPaneInformacoes = new javax.swing.JScrollPane();
        jTableLista = new javax.swing.JTable();
        jLabelUsuario = new javax.swing.JLabel();
        jLabelNomeUsuario = new javax.swing.JLabel();
        jLabelMais = new javax.swing.JLabel();
        jLabelMenos = new javax.swing.JLabel();
        jLabelIgual = new javax.swing.JLabel();
        jLabelValorRenda = new javax.swing.JLabel();
        jLabelValorDespesa = new javax.swing.JLabel();
        jLabelValorSaldo = new javax.swing.JLabel();
        jButtonAdicionarRenda = new javax.swing.JButton();
        jButtonAdicionarDespesa = new javax.swing.JButton();
        jLabelPlanoDeFundo = new javax.swing.JLabel();
        jMenuBarBarraMenu = new javax.swing.JMenuBar();
        jMenuArquivo = new javax.swing.JMenu();
        jMenuItemSair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tela Principal");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelImagem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tela/foto_180_financas.jpeg"))); // NOI18N
        getContentPane().add(jLabelImagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 398, -1, 135));

        jTableLista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Categoria", "Subcategoria", "Valor", "Data"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableLista.setToolTipText("");
        jScrollPaneInformacoes.setViewportView(jTableLista);

        getContentPane().add(jScrollPaneInformacoes, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 740, 100));

        jLabelUsuario.setText("Usuário:");
        jLabelUsuario.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jLabelUsuarioPropertyChange(evt);
            }
        });
        getContentPane().add(jLabelUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabelNomeUsuario.setForeground(new java.awt.Color(18, 20, 124));
        jLabelNomeUsuario.setText("jLabel2");
        getContentPane().add(jLabelNomeUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 270, -1));

        jLabelMais.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        jLabelMais.setText("+");
        getContentPane().add(jLabelMais, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 40, -1, -1));

        jLabelMenos.setFont(new java.awt.Font("Ubuntu", 1, 26)); // NOI18N
        jLabelMenos.setText("-");
        getContentPane().add(jLabelMenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, -1, -1));

        jLabelIgual.setFont(new java.awt.Font("Ubuntu", 0, 20)); // NOI18N
        jLabelIgual.setText("=");
        getContentPane().add(jLabelIgual, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 110, -1, -1));

        jLabelValorRenda.setFont(new java.awt.Font("Ubuntu", 0, 20)); // NOI18N
        jLabelValorRenda.setForeground(new java.awt.Color(21, 152, 1));
        jLabelValorRenda.setText("0.00,00");
        getContentPane().add(jLabelValorRenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 40, -1, -1));

        jLabelValorDespesa.setFont(new java.awt.Font("Ubuntu", 0, 20)); // NOI18N
        jLabelValorDespesa.setForeground(new java.awt.Color(255, 69, 0));
        jLabelValorDespesa.setText("0.00,00");
        getContentPane().add(jLabelValorDespesa, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 70, -1, -1));

        jLabelValorSaldo.setFont(new java.awt.Font("Ubuntu", 0, 20)); // NOI18N
        jLabelValorSaldo.setText("0.00,00");
        getContentPane().add(jLabelValorSaldo, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 110, -1, -1));

        jButtonAdicionarRenda.setText("+ Renda");
        jButtonAdicionarRenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarRendaActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonAdicionarRenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 310, 200, -1));

        jButtonAdicionarDespesa.setText("+ Despesa");
        jButtonAdicionarDespesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarDespesaActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonAdicionarDespesa, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 310, 200, -1));

        jLabelPlanoDeFundo.setIcon(new javax.swing.ImageIcon("/home/matheus/Imagens/fundobranco.png")); // NOI18N
        jLabelPlanoDeFundo.setRequestFocusEnabled(false);
        getContentPane().add(jLabelPlanoDeFundo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 533));

        jMenuArquivo.setText("Sair");

        jMenuItemSair.setText("Sair");
        jMenuItemSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSairActionPerformed(evt);
            }
        });
        jMenuArquivo.add(jMenuItemSair);

        jMenuBarBarraMenu.add(jMenuArquivo);

        setJMenuBar(jMenuBarBarraMenu);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSairActionPerformed
               this.sair();
    }//GEN-LAST:event_jMenuItemSairActionPerformed
    
    public void sair() {
        
        int escolha = JOptionPane.showOptionDialog(null,
            "Deseja realmente sair?",
            "Sair?",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,null,null);
        
        if(escolha == JOptionPane.YES_OPTION)
            this.setVisible(false);
    }
  
    private void jLabelUsuarioPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jLabelUsuarioPropertyChange
       
    }//GEN-LAST:event_jLabelUsuarioPropertyChange

    private void jButtonAdicionarRendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarRendaActionPerformed
        TelaAdicionarRenda renda = new TelaAdicionarRenda(this, true);
        renda.setLocationRelativeTo(null);
        renda.setVisible(true);
        
        Renda umaRenda = new Renda();
        SubcategoriaRenda subcategoria = new SubcategoriaRenda();
        
        subcategoria.setSubcategoria(renda.getINDICE());
        umaRenda.setSubcategoria(subcategoria);
        umaRenda.setValorRecebido(renda.getValor());
        umaRenda.setData(renda.getData());
        
        if(renda.getValor() != 0){
            controleSaldo.adicionarRenda(umaRenda);
            umUsuario.setListaTransacoes(controleSaldo.getListaSaldo());
            this.carregarLista();
            jLabelValorRenda.setText(String.valueOf(df.format(saldoPositivo)));
            saldoPositivo = 0;
            saldoNegativo = 0;
            umUsuario.setSaldo(saldo);
        }
        renda.dispose();
    }//GEN-LAST:event_jButtonAdicionarRendaActionPerformed

    private void jButtonAdicionarDespesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarDespesaActionPerformed
        TelaAdicionarDespesa despesa = new TelaAdicionarDespesa(this, true);
        despesa.setLocationRelativeTo(null);
        despesa.setVisible(true);
        
        
        Despesa umaDespesa = new Despesa();
        SubcategoriaDespesa subcategoria = new SubcategoriaDespesa();
        
        subcategoria.setSubcategoria(despesa.getINDICE());
        umaDespesa.setSubcategoria(subcategoria);
        umaDespesa.setValorRetirado(despesa.getValor());
        umaDespesa.setData(despesa.getData());
        
        if(umaDespesa.getValorRetirado() != 0){
            controleSaldo.adicionarDespesa(umaDespesa);
            umUsuario.setListaTransacoes(controleSaldo.getListaSaldo());
            this.carregarLista();
            jLabelValorDespesa.setText(String.valueOf(df.format(saldoNegativo)));
            saldoNegativo = 0;
            saldoPositivo = 0;
            umUsuario.setSaldo(saldo);
        }
        
        despesa.dispose();     
    }//GEN-LAST:event_jButtonAdicionarDespesaActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                TelaPrincipal dialog = new TelaPrincipal(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionarDespesa;
    private javax.swing.JButton jButtonAdicionarRenda;
    private javax.swing.JLabel jLabelIgual;
    private javax.swing.JLabel jLabelImagem;
    private javax.swing.JLabel jLabelMais;
    private javax.swing.JLabel jLabelMenos;
    private javax.swing.JLabel jLabelNomeUsuario;
    private javax.swing.JLabel jLabelPlanoDeFundo;
    private javax.swing.JLabel jLabelUsuario;
    private javax.swing.JLabel jLabelValorDespesa;
    private javax.swing.JLabel jLabelValorRenda;
    private javax.swing.JLabel jLabelValorSaldo;
    private javax.swing.JMenu jMenuArquivo;
    private javax.swing.JMenuBar jMenuBarBarraMenu;
    private javax.swing.JMenuItem jMenuItemSair;
    private javax.swing.JScrollPane jScrollPaneInformacoes;
    private javax.swing.JTable jTableLista;
    // End of variables declaration//GEN-END:variables
}
