
package tela;

import controlefinancas.ControleUsuario;
import javax.swing.JOptionPane;
import models.Usuario;


public class TelaCadastro extends javax.swing.JDialog {
    
    public TelaCadastro(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
   
   
    private String nome;
    private String login;
    private String senha;
    private String confirmarSenha;
  
    
      
   
    public String getConfirmarSenha() {
        return confirmarSenha;
    }
    
    public String getNome() {
        return this.nome;
    }
     
    public String getLogin() {
        return this.login;
    }
      
    public String getSenha() {
        return this.senha;
    }
    
     
    public void cancelar() {
               
        int escolha = JOptionPane.showOptionDialog(null,
            "Todos os dados serão perdidos. Deseja continuar?",
            "Cancelar?",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,null,null);
        
        if(escolha == JOptionPane.YES_OPTION)
            this.setVisible(false);
             
    }
  

    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private boolean validarCampos() {
        
        senha = new String(jPasswordFieldSenha.getPassword());
        confirmarSenha = new String(jPasswordFieldConfirmarSenha.getPassword());
        
        if (jTextFieldNome.getText().trim().length() == 0) {
            this.exibirInformacao("É obrigatório o preenchimento do campo 'Nome'");
            jTextFieldNome.requestFocus();
            return false;
        }
      
        if (jTextFieldLogin.getText().trim().length() == 0) {
            this.exibirInformacao("É obrigatório o preenchimento do campo 'Login'");
            jTextFieldLogin.requestFocus();
            return false;
        }
       
        if (senha.trim().length() == 0) {
            this.exibirInformacao("É obrigatório o preenchimento do campo 'Senha'");
            jTextFieldLogin.requestFocus();
            return false;
        }
       
        if (confirmarSenha.trim().length() == 0) {
            this.exibirInformacao("É obrigatório o preenchimento do campo 'ConfirmarSenha'");
            jTextFieldLogin.requestFocus();
            return false;
        }
       
        return true;
    }
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelCrieSuaConta = new javax.swing.JLabel();
        jLabelNome = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabelLogin = new javax.swing.JLabel();
        jTextFieldLogin = new javax.swing.JTextField();
        jLabelSenha = new javax.swing.JLabel();
        jPasswordFieldSenha = new javax.swing.JPasswordField();
        jLabelConfirmarSenha = new javax.swing.JLabel();
        jPasswordFieldConfirmarSenha = new javax.swing.JPasswordField();
        jButtonConcluir = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jLabelPlanoDeFundo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tela de Cadastro");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelCrieSuaConta.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jLabelCrieSuaConta.setText("Crie sua conta");
        getContentPane().add(jLabelCrieSuaConta, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 20, 250, 60));

        jLabelNome.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabelNome.setText("Nome");
        getContentPane().add(jLabelNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, 50, -1));
        getContentPane().add(jTextFieldNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, 330, -1));

        jLabelLogin.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabelLogin.setText("Login");
        getContentPane().add(jLabelLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 200, -1, -1));
        getContentPane().add(jTextFieldLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 220, 330, -1));

        jLabelSenha.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabelSenha.setText("Senha");
        getContentPane().add(jLabelSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 270, -1, -1));
        getContentPane().add(jPasswordFieldSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 290, 330, -1));

        jLabelConfirmarSenha.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabelConfirmarSenha.setText("Confirme sua senha");
        getContentPane().add(jLabelConfirmarSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, 150, -1));
        getContentPane().add(jPasswordFieldConfirmarSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 360, 330, -1));

        jButtonConcluir.setText("Concluir");
        jButtonConcluir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonConcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConcluirActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonConcluir, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 420, 330, -1));

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 460, 330, -1));

        jLabelPlanoDeFundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tela/Sem Título-1.png"))); // NOI18N
        getContentPane().add(jLabelPlanoDeFundo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonConcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConcluirActionPerformed
   
        if (this.validarCampos() == false) {
            return;
        }
        
        this.nome = jTextFieldNome.getText();
        this.login = jTextFieldLogin.getText();

        this.senha = new String(jPasswordFieldSenha.getPassword());
        this.confirmarSenha = new String(jPasswordFieldConfirmarSenha.getPassword());
           
        if (senha.equals(confirmarSenha)){
            this.setVisible(false);   
        }      
        else{
            this.exibirInformacao("As senhas não coincidem. Tente novamente!");
            jPasswordFieldConfirmarSenha.setText(null);
        }

    }//GEN-LAST:event_jButtonConcluirActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        this.cancelar();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    
    public static void main(String args[]) {
       
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

    
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                TelaCadastro dialog = new TelaCadastro(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonConcluir;
    private javax.swing.JLabel jLabelConfirmarSenha;
    private javax.swing.JLabel jLabelCrieSuaConta;
    private javax.swing.JLabel jLabelLogin;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelPlanoDeFundo;
    private javax.swing.JLabel jLabelSenha;
    private javax.swing.JPasswordField jPasswordFieldConfirmarSenha;
    private javax.swing.JPasswordField jPasswordFieldSenha;
    private javax.swing.JTextField jTextFieldLogin;
    private javax.swing.JTextField jTextFieldNome;
    // End of variables declaration//GEN-END:variables
}
